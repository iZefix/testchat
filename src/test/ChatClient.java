package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ChatClient {

    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    public void start(String host, int port) {
        try {
            socket = new Socket(host, port);
            in = new BufferedReader(new InputStreamReader(
                    socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
            new Thread(() -> {
                try {
                    String line;
                    while ((line = in.readLine()) != null) {
                        System.out.println("Server> " + line);
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }).start();
            BufferedReader console = new BufferedReader(new InputStreamReader(
                    System.in));
            String cmd;
            while ((cmd = console.readLine()) != null) {
                //wichtig: hier println verwenden
                out.println(cmd);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public static void main(String[] args) {
        ChatClient client = new ChatClient();
        client.start("localhost", 3000);
    }

}
