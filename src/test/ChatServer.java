package test;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ChatServer {

    private Collection<ServerUser> users = new ConcurrentLinkedQueue<>();

    public void start(int port) {
        try {
            ServerSocket server = new ServerSocket(port);
            while (true) {
                Socket sock = server.accept();
                ServerUser serverThread = new ServerUser(sock);
                users.add(serverThread);
                serverThread.start();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ChatServer server = new ChatServer();
        server.start(3000);
    }

}
