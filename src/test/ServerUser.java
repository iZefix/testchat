/*
 * This project is licensed under German Copyright Law. You
 * are NOT allowed to PUBLISH, MODIFY or REDISTRIBUTE this project
 * or parts of it without the explicit permission of the author(s).
 * 
 * Author(s):  Dominik Brosch (Dominik.Brosch@betafase.com)
 * Project:    Test (Test)  
 * File:       ServerUser.java
 * Created on: 22.01.2020
 * 
 */
package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerUser extends Thread {
    
    Socket socket;
    PrintWriter out;
    BufferedReader in;
    
    public ServerUser(Socket socket) {
        this.socket = socket;
    }
    
    @Override
    public void run() {
        try {
            System.out.println("Client connected");
            in = new BufferedReader(new InputStreamReader(
                    socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
            String line;
            sendMessage("hello client!");
            while ((line = in.readLine()) != null) {
                System.out.println("Client> " + line);
            }
            System.out.println("client closed");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                in.close();
                out.close();
                socket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void sendMessage(String message) {
        out.println(message);
    }
    
}
